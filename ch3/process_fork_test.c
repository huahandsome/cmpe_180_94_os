#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

int main() {
    pid_t pid;
    
    // fork a child process
    
    pid = fork();
    
    if ( pid < 0 ) {
        fprintf( stderr, "Fork Failed");
        return 1;    
    }    
    else if ( pid == 0 ) {
        printf("child\n");    
        sleep(10);
        printf("chile exit\n");
    }
    else {
        printf("parent, pid of child %d\n", pid); 
        sleep(5);
        printf("parent exit\n");   
    }

    return 0;
    
}
