#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

int main() {
    pid_t pid;
    
    // fork a child process
    
    pid = fork();
    printf("pid is %d\n", pid);   

    return 0;
} 
