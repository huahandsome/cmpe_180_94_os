#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main ()
{
  pid_t child_pid;

  child_pid = fork ();
  if (child_pid > 0) {
      printf("parent, pid of child:%d, parent pid:%d\n", child_pid, getpid());
      sleep (30);
      exit (0);
  }
  else {
      printf("child exit\n");
      exit (0);
      //sleep (60);
  }
  return 0;
}
