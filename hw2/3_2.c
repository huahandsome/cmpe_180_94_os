///////////////////////////////
// check the result
// pstree -p | grep -A 10 -B 5
//////////////////////////////


#include <stdio.h>
#include <unistd.h>

int main() {
    printf("1st: ppid:%d, pid:%d\n", getppid(), getpid() );    
    /* fork a child process */
    fork();
    printf("2nd: ppid:%d, pid:%d\n",getppid(), getpid());    
    /* fork another child process */
    fork();
    printf("3rd: ppid:%d, pid:%d\n",getppid(), getpid());    
    
    /* and fork another */    
    fork();
    printf("4th: ppid:%d, pid:%d\n",getppid(), getpid() );    

    return 0;
}
