///////////////////////////////
// check the result
// pstree -p | grep -A 10 -B 5
//////////////////////////////

#include <stdio.h>
#include <unistd.h>

int main() {
    int i;

    for ( i = 0; i < 4; i++ )
    {
        fork();
        printf( "i:%d, ppid:%d, pid:%d\n", i, getppid(), getpid() );
    }
    return 0;    
}
