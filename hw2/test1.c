#include <stdio.h>
#include <unistd.h>

int main() {
    //printf("1st: ppid:%d, pid:%d\n", getppid(), getpid() );    
    /* fork a child process */
    pid_t pid = fork();
    printf("2nd: ppid:%d, pid:%d\n",getppid(), getpid());    
    if ( pid == 0 )
    {
        printf("pid of child: %d\n", getpid());    
    }
    /* fork another child process */
    pid = fork();
    
    if ( pid == 0 )
    {
        printf("pid of child: %d\n", getpid() );    
    }
    printf("3rd: ppid:%d, pid:%d\n",getppid(), getpid());    
    
    return 0;
}
