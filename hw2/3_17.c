#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>

#define SIZE 5

int nums[SIZE] = { 0, 1, 2, 3, 4 };

int main() {
    int i ;
    pid_t pid;

    int localVar = 100;
    pid = fork();
    if ( pid == 0 ) {
        for ( i = 0; i < SIZE; i++ ) {
            localVar++;
            nums[i] *= -i;
            //[0, -1, -4, -9, -16]
            printf("CHILD: %d ", nums[i]); /* LINE X */   
            //printf("CHILD: %d ", localVar); /* LINE X */   
        }    
    }
    else if ( pid > 0 ) {
        wait(NULL);    
        for ( i = 0; i < SIZE; i++ )
            printf("PARENT: %d ", nums[i]);    /*LINE Y*/
    }
    
    return 0;    
}
