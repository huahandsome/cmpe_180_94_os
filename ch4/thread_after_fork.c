#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

pthread_t tid;

void* doSomeThing(void* arg){
    printf("in doSomething\n");
    sleep(100);
    return NULL;
}

int main(){
    pid_t pid;
    printf("ppid:%d\n", getppid()); 
       
    int res = pthread_create(&tid, NULL, doSomeThing, NULL);
    if ( res != 0 ){
        printf("Error\n");    
    }

    /**********************************************
     * This code demostrates that if process exit, its threads will also
     * exit. (If sleep(1) is commented out, it hardly to see "in doSomething")
     *********************************************/
    
    sleep(1);
   
    return 0;    
}
