#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

pthread_t tid;

void* doSomeThing(void* arg){
    printf("in doSomething\n");
    sleep(10000);
    return NULL;
}

int main(){
    pid_t pid;
    printf("ppid:%d\n", getppid());    
    pid = fork();

    if ( pid == 0 ) { /* child process */
        printf("1st pid: %d, ppid:%d\n", getpid(), getppid());
        fork();
        printf("2nd pid: %d, ppid:%d\n", getpid(), getppid());
        pthread_create(&tid, NULL, doSomeThing, NULL);
        //sleep(10000);// DONT SLEEP HERE, OTHERWISE CODES BELOW WILL NOT RUN 
    }
    
    printf("3rd pid: %d, ppid:%d\n", getpid(), getppid());
    fork();
    printf("4th pid: %d, ppid:%d\n", getpid(), getppid());
    sleep(10000); 
    return 0;    
}
