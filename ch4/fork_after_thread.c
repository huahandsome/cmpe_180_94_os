#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

pthread_t tid;

void* doSomeThing(void* arg){
    printf("in doSomething\n");
    sleep(10);
    /* when this function return, the new thread is end */
    return NULL;
}

int main(){
    printf("ppid:%d\n", getppid()); 
    
    int res = pthread_create(&tid, NULL, doSomeThing, NULL);
    if ( res != 0 ){
        printf("Error create new thread\n");    
    } 

    fork();
    /*
     * This snip code demostrates that calling fork() after create
     * new thread, it does not create new process in thread.
     * In fact, after running this program, only two processes plus one
     * thread.
     * 
     * If move fork() above pthread_create(), it is two processes plus two
     * thread.
     *
     */    
    printf("after fork: ppid:%d, pid:%d\n", getppid(), getpid() ); 
    printf("Thread will not go to here\n");
    sleep(100000);
   
    return 0;    
}
